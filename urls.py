from django.urls import path, include
from . import views

urlpatterns = [
    path('dashboard/', views.dashboard, name='dashboard'),
    path('register/', views.register, name='register'),
    path('settings/', views.settings, name='settings'),
    path('settings/password/', views.password, name='password'),
    path('', include('django.contrib.auth.urls')),
    path('ajax/validate_username/', views.validate_username, name='validate_username'),
]

